<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::group(['middleware' => 'auth:api'], function(){
    Route::get('current', 'API\UserController@current');
});


Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::post('playlist', 'API\PlaylistController@post')->middleware('auth:api');
Route::get('playlist/{id}', 'API\PlaylistController@get');
Route::get('playlist', 'API\PlaylistController@index');

Route::post('favorite/{playlistId}', 'API\UserController@favorite_playlist_create')->middleware('auth:api');
Route::get('favorite', 'API\UserController@get_favorite_playlists');
Route::delete('favorite', 'API\UserController@delete_favorite_playlist');


Route::post('song', 'API\SongController@post')->middleware('auth:api');
