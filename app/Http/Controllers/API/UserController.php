<?php


namespace App\Http\Controllers\API;

use App\Playlist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\RequiredIf;
use Validator;

class UserController extends Controller
{
    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */

    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            return response()->json(['success' => $success], $this-> successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    /**
     * register api
     *
     * @return Response
     */

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['roles'] = serialize(['ROLE_USER']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $success['name'] =  $user->name;
        return response()->json(['success'=>$success], $this-> successStatus);
    }

    /**
     * current api
     *
     * @return \Illuminate\Http\Response
     */
    public function current()
    {
        $user = Auth::user();
        $user['roles'] = unserialize($user['roles']);
        return response()->json(['success' => $user], $this->successStatus);
    }


    /**
     * creating favorite playlist on User
     *
     * @return Response
     */
    public function favorite_playlist_create(Request $request)
    {
        $user = Auth::user();
        $playlist = Playlist::find($request['playlistId']);
        $user->playlists()->attach($playlist);
        $user->save();
        return response()->json('success', 200);
    }

    /**
     * get favorite playlist by user id
     *
     * return Array[favorites]
     */

    public function get_favorite_playlists(Request $request)
    {
        $user = User::find($request['userId']);
        $favorite_playlists = $user->playlists()->paginate($request['limit']);
        return response()->json($favorite_playlists, 200);
    }

    public function delete_favorite_playlist(Request $request)
    {
        $user = User::find($request['userId']);
        $user->playlists()->detach(Playlist::find($request['playlistId']));
        $user->save();
        return response()->json('success', 200);
    }


}
