<?php
/**
 * Created by PhpStorm.
 * User: hitmanet
 * Date: 31.03.19
 * Time: 17:54
 */

namespace App\Http\Controllers\API;

use App\Song;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Validator;

class SongController extends Controller
{
    public $destinationPath = 'uploads';

    public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'file' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $file = $request->file('file');
        $uniqueName = uniqid().'_'.time().'.'.$file->getClientOriginalExtension();
        $file->storeAs('public/uploads', $uniqueName);
        $input = $request->all();
        $input['file'] = $this->destinationPath.'/'.$uniqueName;
        $song = Song::create($input);
        return response()->json(['data'=>$song], 201);

    }

}
