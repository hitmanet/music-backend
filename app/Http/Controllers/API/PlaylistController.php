<?php
/**
 * Created by PhpStorm.
 * User: hitmanet
 * Date: 30.03.19
 * Time: 16:43
 */


namespace App\Http\Controllers\API;

use App\Playlist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Cocur\Slugify\Slugify;

use Validator;

class PlaylistController extends Controller
{
    public $destinationPath = 'uploads';
    public $successStatus = 200;

    public function index(Request $request){
        $playlists = DB::table('playlists')->paginate($request['limit']);
        return response()->json($playlists, 200);
    }

    /**
     * get playlistapi
     *
     * @return \Illuminate\Http\Response
     */

    public function get(Request $request){
        $playlist = Playlist::find($request['id']);
        return response()->json(['result'=>$playlist], $this->successStatus);
    }

    /**
     * post playlist api
     *
     * @return Response
     */

    public function post(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'required|image'
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 400);
        }
        $file = $request->file('image');
        $uniqueName = uniqid().'_'.time().'.'.$file->getClientOriginalExtension();
        $file->storeAs('public/uploads', $uniqueName);
        $input = $request->all();
        $input['creator'] = $user['id'];
        $input['short_name'] = (new Slugify())->slugify($request['name']);
        $input['image'] = $this->destinationPath.'/'.$uniqueName;
        $playlist = Playlist::create($input);
        return response()->json(['data'=>$playlist], 201);
    }

}
