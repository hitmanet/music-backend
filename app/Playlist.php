<?php
/**
 * Created by PhpStorm.
 * User: hitmanet
 * Date: 30.03.19
 * Time: 16:43
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class Playlist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'image', 'short_name'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }


}
